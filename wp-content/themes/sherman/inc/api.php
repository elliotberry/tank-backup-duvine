<?php

/* ========================================================
 *
 * API function calls for theming
 *
 * ======================================================== */




/* -------------------------------------------------
 * --getting fields
 * ------------------------------------------------- */


if( !function_exists('sk_the_field') ) :
/**
 * theme implementation of ACF's get_field - checks to ensure the
 *  value is there, and then wraps it in html
 *
 * @param string $field : The name of the field
 * @param array  $args  : Arguments to display this field
 *    - id (number)         : the post id to check this field for.
 *    - before (string)     : the html to appear before the field.
 *    - after  (string)     : the html to appear after the field.
 *    - filter (string)     : any filters to apply to the field.
 *    - filter_args (array) : an array of arguments to pass to the filter.
 *    - sub_field (boolean) : whether or not this field is a sub-field of a repeater.
 *    - default (string)    : if the field is undefined, render the default. Default is an empty string.
 *    - return (boolean)    : whether to return the value, or simply echo it. Default is false.
 *    - debug (boolean)     : enables debug mode. Default is false.
 */
function sk_the_field($field, $args = array() ){

    // lets check the existance of ACF
    if ( function_exists( 'get_field' ) === false ){
        return false;
    }

    // ok we're good - let's go
    global $post;

    $defaults = array(
        'id'          => 0,
        'before'      => '',
        'after'       => '',
        'filter'      => '',
        'filter_args' => array(),
        'sub_field'   => false,
        'default'     => '',
        'return'      => false,
        'debug'       => false,
    );

    $options = array_merge($defaults, $args);

    //
    // check to see if we have an ID set. if not, grab it from
    //  the global post variable
    //
    if($options['id']){
        $id = $options['id'];
    } else {
        if( !isset($post->ID)){
            return;
        }
        $id = $post->ID;
    }

    $val = $options['sub_field'] ? get_sub_field($field, $id) : get_field($field, $id) ;

    if( $val ){
        if($options['filter']){
            $val = apply_filters( $options['filter'], $val, $options['filter_args'] );
        }

        $markup = $options['before'] . $val . $options['after'];

        if($options['return']){
            return $markup;
        }

        echo $markup;

    } else if($options['default']){
        $markup = $options['before'] . $options['default'] . $options['after'];

        if($options['return']){
            return $markup;
        }

        echo $markup;
    }
}
endif; // sk_the_field





if( !function_exists('sk_get_field') ) :
/**
 * Wrapper to call the sk_the_field function to return
 *
 * @param string $field : Field we want
 * @param array  $args  : see above
 */
function sk_get_field( $field, $args = array() ){
    $options = array_merge($args, array('return' => true));
    return sk_the_field($field, $options);
}
endif; // sk_get_field





if( !function_exists('sk_the_subfield') ) :
/**
 * Wrapper to call the sk_the_field with a subfield
 *
 * @param string $field : Field we want
 * @param array  $args  : see above
 */
function sk_the_subfield( $field, $args = array() ){
    $options = array_merge( $args, array('sub_field' => true) );
    sk_the_field($field, $options);
}
endif; // sk_the_subfield




if( !function_exists('sk_get_subfield') ) :
/**
 * Wrapper to call the sk_the_field to return and show a subfield
 *
 * @param string $field : Field we want
 * @param array  $args  : see above
 */
function sk_get_subfield( $field, $args = array() ){
    $options = array_merge($args, array('return' => true, 'sub_field' => true));
    return sk_the_field($field, $options);
}
endif; // sk_get_subfield




if( !function_exists('sk_block_field') ) :
/**
 * Wrapper to display a field within a block. Since the page blocks
 *  utilizes an ACF repeater field, this is an alias of sk_the_subfield()
 */
function sk_block_field( $field, $args = array() ){
    sk_the_subfield( $field, $args );
}
endif; // sk_block_field






/* -------------------------------------------------
 *
 * --theme
 *
 * ------------------------------------------------- */


if( !function_exists('sk_the_page_blocks') ) :
/**
 * hooks into the ACF repeater field to render all the additional
 *  page blocks for a page. Calls tempaltes in the blocks/ directory
 */
function sk_the_page_blocks(){

    // check for the existance of ACF
    if ( function_exists( 'have_rows' ) === false ){
        return false;
    }

    // the page blocks repeater field
    $newBlocks = 'sk_page_blocks';

    if( !have_rows( $newBlocks ) ){
        return false;
    }
?>
    <?php // loop through the rows of data ?>
    <?php while ( have_rows($newBlocks) ) : the_row(); ?>
        <?php $block = get_row_layout(); ?>

        <section class="sk-block<?php echo $block ? " block--$block" : ""; ?>">
            <?php
                //
                // - example implementation of getting the header field
                //    for each of the blocks
                //
                // sk_block_field( 'sk_page_block_title' , array(
                //     'before'    => '<header class="page-module-title"><h2>',
                //     'after'     => '</h2></header>'
                // ));

                get_template_part('blocks/block', $block);
            ?>

        </section><!-- .sk-module -->
    <?php endwhile; ?>
<?php
}
endif; // sk_the_page_blocks






/* -------------------------------------------------
 *
 * --renderers
 *     - functions that echo things
 *
 * ------------------------------------------------- */




if( !function_exists( 'duvine_render_tour_loop' ) ) :
/**
 * Wrapper for directly rendering the tourloop
 *
 * @param array $args The WP_Query arguments
 */
function duvine_render_tour_loop( $args = array() ){
    echo duvine_get_tourloop_markup( $args );
}
endif; // duvine_render_tour_loop



if( !function_exists( 'duvine_get_tourloop_markup' ) ) :
/**
 * Gets the list of all tour placards. Returns the markup
 *
 * @param array $args The WP_Query arguments
 *
 * @return string
 */
function duvine_get_tourloop_markup( $args = array() ){

    $options = array_merge( array(
        'duvine_show_count'             => false,
        'duvine_show_you_may_also_like' => false,
        'duvine_show_loadmore'          => true
    ), $args);

    $queryArgs = duvine_build_tour_queryargs( $options );


    // get the number of tours
    if( false === ( $tourCount = get_transient( 'duvine_tour_count' ) ) ){
        $tourCountObj = wp_count_posts( 'tour' );
        if( $tourCountObj && isset( $tourCountObj->publish ) ){
            $tourCount = $tourCountObj->publish;
            set_transient( 'duvine_tour_count', $tourCount, 60 * 60 * 2 );
        }
    }


    // The Query
    $tourQuery = new WP_Query( $queryArgs );

    ob_start();

    // The Loop
?>
    <?php if ( $tourQuery->have_posts() ) : ?>
        
        <?php if( $options['duvine_show_count'] ) : ?>
            <div class="tourlist__count">
                <?php if( $tourCount && $tourCount === $tourQuery->found_posts ) : ?>
                    Showing all tours ( <?php echo $tourCount; ?> )
                <?php else : ?>
                    Showing results ( <?php echo $tourQuery->found_posts; ?> )
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <ul class="tourlist baselist">
            <?php while ( $tourQuery->have_posts() ) : $tourQuery->the_post(); ?>
                <?php duvine_render_tour_placard(); ?>
            <?php endwhile; ?>
        </ul>

        <?php if( $options['duvine_show_loadmore'] && $tourQuery->found_posts > $tourQuery->post_count ) : ?>
            <div class="t-center tourloop__footersection"><a href="#" class="cta js-trigger-tourloop-lazyload cta--hasloader" data-tourcount="<?php echo $tourQuery->found_posts; ?>" data-touroffset="<?php echo $tourQuery->post_count; ?>">Load More</a></div>
        <?php endif; ?>
        
    <?php else : ?>
        <div class="notours">
            <p>No tours found.</p>
        </div>
    <?php endif; ?>

    <?php
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>


    <?php if( $options['duvine_show_you_may_also_like'] && $tourQuery->found_posts < 3 ) : ?>
        <?php
            $displayed_tours = wp_list_pluck( $tourQuery->posts, 'ID' );

            $youMayAlsoLikeArgs = array(
                'posts_per_page' => 2,
                'post_type'      => 'tour',
                'orderby'        => 'RAND'
            );

            if( $displayed_tours ){
                $youMayAlsoLikeArgs['post__not_in'] = $displayed_tours;
            }

            if( $q_level = get_query_var('level')){
                
                $levels_meta_query = array('relation' => 'OR');
                foreach( $q_level as $level ){
                    if( $level > 0 ){
                        array_push( $levels_meta_query, array(
                            'key'     => 'd_cycling_level',
                            'value'   => $level,
                            'compare' => 'LIKE'
                        ));
                    } else {
                        array_push( $levels_meta_query, array(
                            'key'     => 'd_non_rider',
                            'value'   => '1',
                            'compare' => '=='
                        ));
                    }
                    
                }

                $youMayAlsoLikeArgs['meta_query'] = $levels_meta_query;
            }

            $youMayAlsoLike = get_posts( $youMayAlsoLikeArgs );
        ?>

        <?php if( $youMayAlsoLike ) : ?>
            <div class="tourloop__footersection">
                <h3 class="blockheader">You may also like</h3>
                <ul class="tourlist baselist">
                    <?php foreach( $youMayAlsoLike as $secondaryTour ) : ?>
                        <?php duvine_render_tour_placard( $secondaryTour ); ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php endif; ?>

<?php

    return ob_get_clean();
}
endif; // duvine_get_tourloop_markup






if( !function_exists( 'duvine_render_grouped_tour_loop' ) ) :
/**
 * Lists tours based on the tour arguments, with filtering and grouped by tour location
 *
 * @param array $args     The WP_Query arguments
 * @param int   $depth    Depth of the location
 */
function duvine_render_grouped_tour_loop( $args, $depth = 1 ){
    $defaults = array(
        'post_type' => 'tour',
        'orderby'   => array( 'title' => 'ASC'),
    );

    $queryArgs = array_merge( $defaults, $args );

    // The Query
    $tourList = get_posts( $queryArgs );
    $tourQuery = duvine_organize_tours_by_location( $tourList, $depth );
?>
    <?php // The Loop ?>
    <?php if ( $tourQuery ) : ?>
        <?php $tour_locations = count( $tourQuery ); ?>
        <?php uksort($tourQuery, 'duvine_sort_tours_by_location'); ?>

        <div class="tourloop__quickfilter">
            <span class="filtercell__label">Destinations</span>

            <select class="js-duvine-chosen quickfilter__filters" data-placeholder="Filter by region">
                <option value="all">All destinations</option>
                <?php foreach( $tourQuery as $loc => $tours ) : ?>
                    <option value="<?php echo $loc; ?>"><?php echo get_the_title($loc); ?></option>
                <?php endforeach; ?>
            </select>

        </div>

        <div class="tourloop__holder">
            <?php foreach( $tourQuery as $loc => $tours ) : ?>
                <div class="tourlist__country" data-country="<?php echo $loc; ?>">
                    <h3 class="blockheader tourlist__countryname"><?php echo get_the_title($loc); ?></h3>
                    <?php if( $tours ) : ?>
                        <ul class="tourlist baselist">
                            <?php foreach ( $tours as $index => $tour ) : ?>
                                <?php duvine_render_tour_placard( $tour ); ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php else : ?>
        <p>No tours found.</p>
    <?php endif; ?>
<?php
}
endif; // duvine_render_grouped_tour_loop






if( !function_exists( 'duvine_sort_tours_by_location' ) ) :
/**
 * Sorts the given tour by location
 */
function duvine_sort_tours_by_location( $a, $b ){
    $a_title = get_the_title( $a );
    $b_title = get_the_title( $b );
    
    return strcmp( $a_title, $b_title );
}
endif; // duvine_sort_tours_by_location









if( !function_exists('duvine_render_tour_placard') ) :
/**
 * Renders the location placard
 */
function duvine_render_tour_placard( &$_post = null ){
    global $post;

    if( $_post !== null ){
        $post = $_post;

        setup_postdata( $post );
    }

    $taxfilters = duvine_get_active_filters( $post );
    $permalink  = get_the_permalink();
?>
    <li class="tourlist__listing tourlist__listing--<?php echo get_field('d_private_only') ? 'private' : 'scheduled' ;?>">
        <div class="tourlisting__image"><a href="<?php echo $permalink; ?>">
            <?php
                sk_the_field('d_thumbnail_image', array(
                    'filter'      => 'sk_img_markup',
                    'filter_args' => array(
                        'img_size' => 'tour_thumbnail'
                    ),
                    'default'     => '<div class="tourlisting__placeholder"></div>'
                ));
            ?>
        </a></div><!-- .tourlisting__image -->

        <div class="tourlisting__data">
            <?php duvine_tour_flags(); ?>

            <header>
                <div class="tourlisting__location"><?php duvine_tour_breadcrumbs( $post->ID, true ); ?></div>
                <h3 class="blockheader"><a href="<?php echo $permalink; ?>"><?php the_title(); ?></a></h3>
            </header>

            <?php
                sk_the_field('d_tour_subtitle', array(
                    'before' => '<div class="tourlisting__description">',
                    'after'  => '</div>'
                ));
            ?>

            <div class="tourlisting__meta">
                <?php
                    sk_the_field('d_tour_duration', array(
                        'before' => '<span class="tourmeta tourmeta--separator">',
                        'after'  => '</span>',
                        'filter' => 'duvine_tourlength_strip_nights'
                    ));
                    sk_the_field('d_cycling_level', array(
                        'before' => '<span class="tourmeta tourmeta--separator">',
                        'after'  => '</span>',
                        'filter' => 'duvine_cycling_level'
                    ));

                    if( $minPrice = duvine_get_minimum_price() ){
                        echo '<span class="tourmeta tourmeta--separator"><strong>Price From: </strong>' . $minPrice . '</span>';
                    }
                ?>
            </div>
            
            <?php if( $taxfilters ) : ?>
                <p class="tourlisting__filters"><?php echo $taxfilters; ?></p>
            <?php endif; ?>


            <div class="tourlisting__footer l-cf">
                <a class="tourlisting__datetrigger js-open-datedrawer" href="#">View dates</a>
                <a class="cta tourlisting__detaillink" href="<?php echo $permalink; ?>">View Tour</a>
            </div>

        </div><!-- .tourlisting__data -->

        <div class="tourlisting__datedrawer">
            <?php duvine_render_tour_date_drawer(); ?>
        </div>
    </li>
<?php
    if( $_post !== null ){
        wp_reset_postdata();
    }
}
endif; // duvine_render_tour_placard





if( !function_exists( 'duvine_render_tour_date_drawer' ) ) :
/**
 * Renders the date drawer
 */
function duvine_render_tour_date_drawer(){
    global $post;

    $tourDates = duvine_get_tour_dates();

    $privateOnly = get_field('d_private_only');
    $dates = get_field('d_tour_schedules');
?>

    <div class="datedrawer">
        <?php if( $tourDates ) : ?>
            <div class="tourdates dateslider" data-yearsperslide="2">
                <div class="dateslider__sliderwrap">
                    <div class="dateslider__slider">
                        <?php foreach( $tourDates as $year => $dates ) : ?>
                            <div class="dateslider__el">
                                <div class="dateslider__elheader">
                                    <span class="dateslider__eltitle"><?php echo $year; ?></span>
                                </div>
                                <ul class="dateslider__datelist">
                                    <?php foreach( $dates as $date ) : ?>
                                    <?php $flag = $date['flags'] ? $date['flags']['value'] : ''; ?>
                                        <li class="dateslider__date <?php if( $flag === 'sold_out' ) echo ' dateslider__date--soldout'; ?>"><?php echo $date['label']; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php elseif( !$privateOnly ) : ?>
            <?php duvine_text_dates_coming_soon(); ?>
        <?php endif; ?>
        
        <?php
            if(duvine_is_family_tour()){
                duvine_text_family_tour_date_dropdown();
            } elseif($privateOnly){
                duvine_text_private_tour_date_dropdown();
            } else {
                duvine_text_scheduled_tour_date_dropdown();
            }
        ?>
    </div>

<?php
}
endif; // duvine_render_tour_date_drawer





if( !function_exists( 'duvine_render_tourgallery' ) ) :
/**
 * Renders the tourgallery
 *
 * @param array $gallery  The gallery array from WP
 */
function duvine_render_tourgallery( $gallery ){

    if( ! $gallery ){
        return false;
    }

    echo '<div class="tourgallery">';

    foreach( $gallery as $img ) {
        $width  = $img['width'];
        $height = $img['height'];
        $ratio  = $height / $width;
        $renderArgs = array( 'img_size' => 'full', 'container_class' => 'tourgallery__image' );

        if( $width < 960 && $ratio < .7 ){
            $renderArgs['img_size'] = 'tourgallery_rectangle';
            $renderArgs['container_class'] = 'tourgallery__image tourgallery__image--rectangle';
        } else if( $width < 960 ){
            $renderArgs['img_size'] = 'tourgallery_square';
            $renderArgs['container_class'] = 'tourgallery__image tourgallery__image--square';
        }

        duvine_render_img( $img, $renderArgs );

    }

    echo '</div>';
}
endif; // duvine_render_tourgallery






if( !function_exists( 'duvine_render_featuredin' ) ) :
/**
 * Renders the publications that are included in the given list of press posts
 *
 * @param array $press, The list of press posts
 */
function duvine_render_featuredin( $press ){

    if( !$press ){
        return false;
    }

    // hold the pubs
    $publications = array();
    $awards = array();

    if($press) {

    echo '<div class="featuredin">';
    echo    '<h6 class="capsheader">As featured in:</h6>';
    echo    '<ul class="menu">';

        // foreach( $press as $pressPost ){

        //     // get awards
        //     if( $award = get_field('d_press_award_logo', $pressPost->ID) ){
        //         $awardLogo = duvine_get_url_from_object( $award );
        //         $awardURL = get_field('d_press_url',$pressPost->ID);

        //         echo '<li class="featuredin__logo featuredin__logo--awards">';
        //             echo '<a target="_blank" href="' . $awardURL . '">';
        //                 echo '<img src=' . $awardLogo . '>';
        //             echo '</a>';
        //         echo '</li>';
        //     }
        // }

        foreach( $press as $press_item ){
            if( $pub = get_field('d_press_publication', $press_item->ID) ){
                $pub_name = $pub->post_name;
                if(get_field('d_press_award_logo', $press_item->ID)) {
                    $logo = get_field('d_press_award_logo', $press_item->ID)['url'];
                } else {
                    $logo = duvine_get_url_from_object( get_post_thumbnail_id( $pub ), 'full' );
                }

                $url = duvine_get_press_url($press_item);

                echo "<li class=\"featuredin__logo featuredin__logo--$pub_name\">";

                if( $url ){
                    echo "<a href=\"$url\" target=\"_blank\">";
                }

                echo "<img src=\"$logo\" alt=\"$pub_name\">";

                if( $url ){
                    echo "</a>";
                }

                echo "</li>";
            }
        }

    echo    '</ul>';
    echo '</div>';

    }

    if( !$press && !$awards){
        return false;
    }



    // foreach( $awards as $awardLogo ){
    //     echo '<li class=\"featuredin__logo featuredin__logo--awards\">';
    //     echo '<img src=\"$awardLogo\">';
    //     echo '</li>';
    // }


}
endif; // duvine_render_featuredin







if( !function_exists( 'duvine_list_tour_tags' ) ) :
/**
 * Lists out all of the terms in the given taxonomy
 *
 * @param string $tax   The taxonomy to get all of the tags from
 * @param string $label Label for the filter list
 */
function duvine_list_tour_tags( $tax, $label = '' ){

    $q_terms = get_query_var( 'taxfilter_' . $tax );

    $terms = get_terms( array(
        'taxonomy'   => $tax,
        'hide_empty' => false
    ));

    if( ! $terms || isset( $terms->errors ) ){
        return false;
    }

    $label = $label === '' ? ucfirst($tax) : $label;
?>

    <div class="filtercell filtercell--taxonomy filtercell--<?php echo $tax; ?>">
        <span class="filtercell__label"><?php echo $label; ?></span>
        <ul class="baselist">
            <?php foreach( $terms as $term ) : ?>
                <?php $checked = $q_terms && in_array( $term->term_id, $q_terms ) ? ' checked' : ''; ?>
                <li class="filterterm">
                    <input type="checkbox" id="filter--<?php echo $term->slug; ?>" name="taxfilter_<?php echo $tax; ?>[]" value="<?php echo $term->term_id; ?>"<?php echo $checked; ?>>
                    <label for="filter--<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php
}
endif; // duvine_list_tour_tags





if( !function_exists( 'duvine_date_picker_input' ) ) :
/**
 * Renders the date picker
 */
function duvine_date_picker_input(){
    $q_dates = get_query_var('date');
?>
    <div class="chosen-container chosen-container-multi js-duvine-datepicker" style="width: 100%;">
        <ul class="chosen-choices chosen-date datepicker__trigger">
            <li class="search-field"<?php if( $q_dates ) echo ' style="width: 0; overflow:hidden;"'; ?>>
                <span class="chosen-date-placeholder">Select months and years</span>
            </li>
            <?php if( $q_dates ) : ?>
                <?php foreach( $q_dates as $d ) : ?>
                    <?php $year = substr($d, 0, 4); ?>
                    <?php $month = apply_filters('map_month', substr($d, 4), true ); ?>
                    <?php $label = $month . ' ' . $year; ?>
                    <li class="search-choice" data-label="<?php echo $label; ?>"><span><?php echo $label; ?></span><a class="search-choice-close"></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <div class="chosen-drop">
            <?php duvine_render_dateslider(); ?>
        </div>
    </div>
<?php
}
endif; // duvine_date_picker_input





if( !function_exists( 'duvine_render_dateslider' ) ) :
/**
 * Renders the dateslider
 *
 * @param int $yearsPerSlide The number of years that appear in the carousel
 */
function duvine_render_dateslider( $yearsPerSlide = 1 ){

    $currentYear = date('Y');

    // get the number of years in the future we should show
    $currentMonth = date('n');
    $yearsForward = $currentMonth > 9 ? 2 : 1;

    $years = array( $currentYear );
    $months = array( 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );

    for( $i = 1; $i <= $yearsForward; $i++ ){
        array_push($years, $currentYear + $i );
    }

    $q_dates = get_query_var('date');
?>
    <div class="dateslider" data-yearsperslide="<?php echo $yearsPerSlide; ?>">
        <div class="dateslider__sliderwrap">
            <div class="dateslider__slider">
                <?php foreach( $years as $year ) : ?>
                    <div class="dateslider__el">
                        <div class="dateslider__elcontainer">
                            <header class="dateslider__elheader">
                                <span class="dateslider__eltitle"><?php echo $year; ?></span>

                                <?php if( $yearsPerSlide > 1 ) : ?>
                                    <span class="checkmark--white checkmark--small dateslider__option--selectall"><input type="checkbox" id="dateslider--<?php echo $year; ?>--selectall"><label class="tourfinderbanner__label" for="dateslider--<?php echo $year; ?>--selectall">Select all months</label></span>
                                <?php endif; ?>
                            </header>
                            <ul class="dateslider__dateblocks">
                                <?php foreach( $months as $mIndex => $month ) : ?>
                                    <?php $dateId = "datepicker--$month-$year"; ?>
                                    <?php $queryString = $year . sprintf("%02d", $mIndex + 1); ?>
                                    <?php $displayDate = "$month $year"; ?>
                                    <?php $checked = $q_dates && in_array($queryString, $q_dates) ? ' checked' : ''; ?>
                                    <?php 
                                    $monthClass = '';

                                    if($queryString < date('Ym')) {
                                        $monthClass = 'passed';
                                    }
                                    ?>

                                    <?php

                                    if(get_posts())

                                    ?>

                                    <li class="dateslider__monthblock"><input data-display="<?php echo $displayDate; ?>" id="<?php echo $dateId; ?>" type="checkbox" name="date[]" value="<?php echo $queryString; ?>"<?php echo $checked; ?>><label for="<?php echo $dateId; ?>" class="<?php echo $monthClass; ?>"><span><?php echo $month; ?></span></label></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php
}
endif; // duvine_render_dateslider






if( !function_exists( 'duvine_tour_flags' ) ) :
/**
 * Output the tour flags
 */
function duvine_tour_flags(){
    global $post;
    
    $guaranteed  = get_field('d_guaranteeed_to_run');
    $newtour     = get_field('d_new_tour');
    $privatetour = get_field('d_private_only');
    $comingsoon  = get_field('d_tour_coming_soon');

    if( $newtour || $privatetour || $comingsoon ) {
        echo '<div class="infobox__flags">';
            if( $newtour ) echo '<span class="infobox__flag">New Tour</span>';
            if( $privatetour ) echo '<span class="infobox__flag">Private only</span>';
            if( $comingsoon) echo '<span class="infobox__flag">Coming soon</span>';
        echo '</div>';
    }
}
endif; // duvine_tour_flags










if( !function_exists( 'duvine_tour_breadcrumbs' ) ) :
/**
 * Render breadcrumbs for a single tour
 *
 * @param int     $postId ID of the post in wordpress
 * @param boolean $link   Whether or not to link the breadcrumbs
 */
function duvine_tour_breadcrumbs( $postId = null, $link = false ){

    if( $postId === null ){
        global $post;
        $postId = $post->ID;
    }


    $locations = get_field('d_tour_location', $postId);

    if( ! $locations ){
        return false;
    }

    $breadcrumbs = '<ul class="breadcrumbs baselist">';

    foreach( $locations as $locID ){
        $locTitle = get_the_title( $locID );
        $link = get_the_permalink( $locID );
        $breadcrumbs .= '<li class="breadcrumb__item">';
        $breadcrumbs .= $link ? '<a class="breadcrumb__link basiclink" href="' . get_the_permalink( $locID ) . '">' . $locTitle . '</a>' : $locTitle;
        $breadcrumbs .= '</li>';
    }

    $breadcrumbs .= '</ul>';

    echo $breadcrumbs;
}
endif; // duvine_tour_breadcrumbs






if( !function_exists( 'duvine_region_breadcrumbs' ) ) :
/**
 * Renders breadcrumbs for the current region
 */
function duvine_region_breadcrumbs(){
    global $post;

    $ancestors = get_ancestors( $post->ID, 'location', 'post_type' );

    if( ! $ancestors ){
        return false;
    }

    $breadcrumbs = '<ul class="breadcrumbs baselist">';

    $currTitle = get_the_title();
    $reversedAncestors = array_reverse( $ancestors );

    foreach( $reversedAncestors as $ancestor ){
        $ancestorTitle = get_the_title( $ancestor );
        $ancestorLink = get_the_permalink( $ancestor );
        $breadcrumbs .= "<li class=\"breadcrumb__item\"><a class=\"breadcrumb__link\" href=\"$ancestorLink\">$ancestorTitle</a></li>";
    }

    $breadcrumbs .= "<li class=\"breadcrumb__item\">$currTitle</li>";
    $breadcrumbs .= '</ul>';

    echo $breadcrumbs;
}
endif; // duvine_region_breadcrumbs







if( !function_exists( 'duvine_accordion_element' ) ) :
/**
 * Builds an individual accordion element
 */
function duvine_accordion_element( $title, &$content = '' ){
?>
    <li>
        <header class="accordion__header accordion__header--showlesslabel js-duvine-accordion-trigger">
            <?php echo $title; ?>
        </header>
        <div class="accordion__content">
            <div class="accordion__contentwrapper d-content">
                <?php echo $content; ?>
            </div>
        </div>
    </li>
<?php
}
endif; // duvine_accordion_element






if( !function_exists( 'duvine_render_homepage_hero' ) ) :
/**
 * Renders the homepage hero
 */
function duvine_render_homepage_hero(){

    $heroType = get_field('d_hero_type');

    $autoscroll = get_field('d_hero_autoscroll_speed');
?>
    <div class="duvine-homepage-hero" data-autoscroll="<?php echo $autoscroll; ?>">

        <?php
            $heroCopy = sk_get_field('d_hero_copy', array(
                'before' => '<h2 class="hero__title pageheader">',
                'after'  => '</h2>'
            ));
        ?>

        <?php if( $heroType === 'dayslider' ) : ?>
            <?php
                $morning   = get_field('d_hero_morning');
                $afternoon = get_field('d_hero_afternoon');
                $evening   = get_field('d_hero_evening');
                $night     = get_field('d_hero_night');

                $morning_label   = get_field('d_hero_morning_label');
                $afternoon_label = get_field('d_hero_afternoon_label');
                $evening_label   = get_field('d_hero_evening_label');
                $night_label     = get_field('d_hero_night_label');

                $imgArgs = array(
                    'img_size'        => 'banner_hero',
                    'container_class' => 'heroslide__slide bannerhero__image'
                );

                $activeImgArgs = array_merge( $imgArgs, array('container_class' => 'heroslide__slide heroslide__slide--active bannerhero__image') );
            ?>
            <div class="dayslider bannerhero">
                <div class="overlay"></div>
                <div class="heroslides">
                    <?php duvine_render_img( $morning, $activeImgArgs ); ?>
                    <?php duvine_render_img( $afternoon, $imgArgs ); ?>
                    <?php duvine_render_img( $evening, $imgArgs ); ?>
                    <?php duvine_render_img( $night, $imgArgs ); ?>
                </div>
            </div>

            <div class="hero__center">

            <?php if(get_field('d_hero_cta_link')) { ?>
                    <a style="display: block;" href="<?php the_field('d_hero_cta_link'); ?>"><?php echo $heroCopy; ?> </a>
                <?php } ?>
                
                    <ul class="dayslider__nav menu" <?php if(!get_field('show_navigation')) { echo 'style="visibility:hidden;height:0;margin:0;"'; } ?>>
                        <?php if( $morning ) : ?><li class="dayslider__time dayslider__time--active"><?php echo $morning_label; ?></li><?php endif; ?>
                        <?php if( $afternoon ) : ?><li class="dayslider__time"><?php echo $afternoon_label; ?></li><?php endif; ?>
                        <?php if( $evening ) : ?><li class="dayslider__time"><?php echo $evening_label; ?></li><?php endif; ?>
                        <?php if( $night ) : ?><li class="dayslider__time"><?php echo $night_label; ?></li><?php endif; ?>
                    </ul>

               
                    <?php if(get_field('d_hero_cta_text')) { ?>
                        <a class="hero-cta" href="<?php the_field('d_hero_cta_link'); ?>"><?php the_field('d_hero_cta_text'); ?></a>
                    <?php } ?>
               
            </div>
        <?php else : ?>
            <div class="bannerhero">
                <?php 
                if( get_field('desktop_header_before_video_load') && get_field('mobile_header_replacement') ) : ?>
                    <style>
                   .background-image {
                        background-image:url('<?php the_field('desktop_header_before_video_load') ?>');
                    }
                    @media only screen and ( max-width: 580px ) {
                        .background-image {
                            background-image:url('<?php the_field('mobile_header_replacement') ?>');
                        }
                    }
                    </style>
                <?php endif; ?>
                <div class="overlay"></div>
                <div class="background-image"></div>
                <?php if((get_field('d_hero_type') == 'video') && get_field('d_hero_video_homepage')) { ?>
                    <video data="<?php the_field('d_hero_video_homepage'); ?>" loop="true" autoplay="autoplay" id="vid" muted onended="this.play();" playsinline>
                      
                    </video>
                <?php } else { ?>

                    <?php
                        duvine_render_img( get_field('d_hero_image_homepage'), array(
                            'img_size'        => 'banner_hero',
                            'container_class' => 'bannerhero__image'
                        ));
                    ?>
                    
                    <?php
                        duvine_render_img( get_field('d_hero_overlaid_image'), array(
                            'container_class' => 'bannerhero__overlayimage'
                        ));
                    ?>

                <?php } ?>

                
            </div>
            <div class="hero__center">
                <?php if(get_field('d_hero_cta_text')) { ?>
                    <a style="display: block;" href="<?php the_field('d_hero_cta_link'); ?>">
                <?php } ?>

                <?php echo $heroCopy; ?>
                
                <?php if(get_field('show_hero_copy_link') && get_field('hero_copy_link')) { ?>
                    </a>
                <?php } ?>
                
                
                    <?php if(get_field('d_hero_cta_text')) { ?>
                        <a class="hero-cta" href="<?php the_field('d_hero_cta_link'); ?>"><?php the_field('d_hero_cta_text'); ?></a>
                    <?php } ?>
               
            </div>
        <?php endif; ?>
    </div>
<?php
}
endif; // duvine_render_homepage_hero





if( !function_exists( 'duvine_render_page_hero' ) ) :
/**
 * description
 */
function duvine_render_page_hero(){
    global $post;

    duvine_render_img( get_field('d_page_hero'), array(
        'img_size'        => 'banner_hero_page',
        'container_class' => 'pagehero'
    ));
}
endif; // duvine_render_page_hero





if( !function_exists( 'duvine_get_img' ) ) :
/**
 * Gets an image tag, and returns it
 *
 * @param array $imgField The image from the CMS database
 * @param array $args     Arguments
 */
function duvine_get_img( $imgField, $args = array() ){
    $defaults = array(
        'img_size'        => 'full',
        'container_class' => '',
        'container'       => true
    );

    $options = array_merge( $defaults, $args );

    $imgSrc = duvine_get_url_from_object( $imgField, $options['img_size'] );

    if( !$imgSrc ){
        return false;
    }
    
    $markup = '';

    if( $options['container'] === true ){
        $containerClass = $options['container_class'] ? ' class="' . $options['container_class'] . '"' : '';
        $markup .= "<div$containerClass>";
    }

    $markup .= "<img src=\"$imgSrc\" alt=\"\">";

    if($imgField['caption'] && (get_post_type() != 'tour')) {
        $markup .= '<p class="image__caption">' . $imgField['caption'] . '</p>';
    }

    if( $options['container'] === true ){
        $markup .= '</div>';
    }

    return $markup;
}
endif; // duvine_get_img


if( !function_exists( 'duvine_render_img' ) ) :
/**
 * Renders an image, given an image array. If the image array is null, then returns nothing
 *
 * @param array $imgField The image from the CMS database
 * @param array $args     Arguments
 */
function duvine_render_img( $imgField, $args = array() ){

    $img = duvine_get_img( $imgField, $args );
    echo $img;
}
endif; // duvine_render_img






if( !function_exists( 'duvine_render_cta' ) ) :
/**
 * Renders a CTA
 */
function duvine_render_cta(){
    global $post;

    $cta = get_sub_field('d_cta');

    if( ! $cta ){
        return false;
    }

    $ctaText = $cta['title'] ?: 'Learn more';
    $ctaUrl = $cta['url'];

    echo "<a href=\"$ctaUrl\" class=\"cta\">$ctaText</a>";
    
}
endif; // duvine_render_cta






if( !function_exists( 'duvine_render_social_icons' ) ) :
/**
 * Renders the social icons
 *
 * @param (string) $additionalClasses, Any classes to add to the container
 */
function duvine_render_social_icons( $additionalClasses = '' ){
    global $post;

    $postlink = urlencode(get_the_permalink());
?>
    <div class="social-icons__wrapper <?php echo $additionalClasses; ?>">
        <ul class="menu">
            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $postlink; ?>"><?php include_svg('social--facebook'); ?></a></li>
            <li><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $postlink; ?>&text="><?php include_svg('social--twitter'); ?></a></li>
            <li><a target="_blank" href="https://plus.google.com/share?url=<?php echo $postlink; ?>"><?php include_svg('social--googleplus'); ?></a></li>
            <li><a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo $postlink; ?>&media=&description="><?php include_svg('social--pinterest'); ?></a></li>
            <li><a href="mailto:?body=<?php echo $postlink; ?>&subject=Check out this tour from DuVine" class="social--email"><?php include_svg('social--email'); ?></a></li>
            <li><a href="#" onclick="window.print(); return false;" class="social--print"><?php include_svg('social--print'); ?></a></li>
        </ul>
    </div>
<?php
}
endif; // duvine_render_social_icons






if( !function_exists( 'duvine_render_tourmap' ) ) :
/**
 * Renders the tour map and the markers
 */
function duvine_render_tourmap(){
    $tourstops = get_field('d_tour_stops');

    if( ! $tourstops ){
        return false;
    }

    $data_center = '';
    if( $center = get_field('d_tour_weather_address') ) {
        $centerLat = $center['lat'];
        $centerLng = $center['lng'];
        $data_center = "[$centerLat,$centerLng]";
    }
?>
    <div class="tourmap">
        <div id="d-tourmap" class="tourmap__map" data-center='<?php echo $data_center; ?>'></div>
        
        <div class="mappoints">
            <?php foreach( $tourstops as $stop ) : ?>
                <?php $lat = $stop['tour_stop']['lat']; ?>
                <?php $lng = $stop['tour_stop']['lng']; ?>
                <?php $point = "[$lat,$lng]"; ?>
                <span class="mappoint" data-point='<?php echo $point; ?>'></span>
            <?php endforeach; ?>
        </div>
    </div>
    
<?php
}
endif; // duvine_render_tourmap





if( !function_exists( 'duvine_render_publication_logo' ) ) :
/**
 * Renders the publication logo
 *
 * @param object | int $publication, The publication we want the logo for
 */
function duvine_render_publication_logo( $publication ){
    if( ! $publication ){
        return false;
    }

    $logo = duvine_get_url_from_object( get_post_thumbnail_id( $publication ), 'full' );

    if( ! $logo ){
        return false;
    }

    $pubTitle = get_the_title($publication);
    $pubSlug = $publication->post_name;

    echo "<div class=\"pressblock__logo pressblock__logo--$pubSlug\">";
    echo    "<img src=\"$logo\" alt=\"$pubTitle\">";
    echo "</div>";
}
endif; // duvine_render_publication_logo






if( !function_exists( 'duvine_render_mobilenav' ) ) :
/**
 * Renders the mobile navigation
 */
function duvine_render_mobilenav(){
    $locationlist = duvine_get_hierarchical_location_list();

    $collections = get_posts( array(
        'post_type'      => 'tour_collection',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'post_parent'    => 0
    ));

?>

    <div id="duvine-mobile-nav" class="mobilenav">
        <div class="mobilenav__container">
            <div class="mobilenav__mainpane">

                <div class="mobilenav__section">
                    <ul class="blockmenu">
                        <li class="menu-item menu-item-has-children"><a href="#">Search</a>
                            <ul class="sub-menu">
                                <li class="menu-item">
                                    <form method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                        <label for="s" class="assistive-text"><?php _e( 'Search', 'twentyeleven' ); ?></label>
                                        <input type="search" name="s" placeholder="<?php esc_attr_e( 'Search', 'twentyeleven' ); ?>" value="<?php echo get_search_query(); ?>" class="mobilesearch" />
                                        <button type="submit" class="searchform__submit searchform__submit--mobile" id="searchsubmit">Search</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>


                <?php if( $locationlist ) : ?>
                    <div class="mobilenav__section">
                        <h4 class="mobilenav__header">Destinations</h4>
                        <ul class="blockmenu">
                            <?php foreach( $locationlist as $index => $continent ) : ?>
                                <li class="menu-item">
                                    <span class="subdrawer__trigger"><?php echo $continent['continent']; ?></span>
                                    
                                    <?php if( $continent['countries'] ) : ?>
                                        <div class="subdrawer sub-menu">
                                            <ul class="blockmenu">
                                                <?php foreach( $continent['countries'] as $country ) : ?>
                                                    <li class="menu-item"><a href="<?php echo get_the_permalink( $country->ID ); ?>"><?php echo $country->post_title; ?></a></li>
                                                <?php endforeach; ?>
                                                <li class="menu-item"><a href="<?php echo get_the_permalink($continent['id']); ?>">View all of <?php echo $continent['continent']; ?></a></li>
                                            </ul>
                                            
                                        </div>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <?php if( $collections ) : ?>
                    <div class="mobilenav__section">
                        <h4 class="mobilenav__header">Tour Collections</h4>
                        <ul class="blockmenu">
                            <?php foreach( $collections as $coll ) : ?>
                                <li class="menu-item">
                                    <a href="<?php echo get_the_permalink( $coll->ID ); ?>"><?php echo $coll->post_title; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <?php
                    wp_nav_menu( array(
                        'theme_location'  => 'primary',
                        'container_class' => 'mobilenav__section',
                        'menu_class'      => 'blockmenu',
                        'duvine_location' => 'mobilenav'
                    ) );
                ?>

            </div><!-- mobilenav__mainpane -->

            <a href="#" class="subdrawer__back js-mobilenav-back">Back</a>
            <a href="#" class="mobilenav__close js-close-mobilenav">Close</a>
        </div><!-- mobilenav__container -->


    </div>
<?php 
}
endif; // duvine_render_mobilenav





if( !function_exists( 'duvine_render_search_modal' ) ) :
/**
 * Renders the search modal
 */
function duvine_render_search_modal(){
?>
    <div class="searchmodal" id="duvine-searchmodal">
        <div class="l-container l-container--small"><?php get_search_form() ?></div>
        <a href="#" class="closesearch"></a>
    </div>
<?php
}
endif; // duvine_render_search_modal

if( !function_exists( 'duvine_render_newsletter_modal' ) ) :
/**
 * Renders the search modal
 */
function duvine_render_newsletter_modal(){
?>
    <div class="newslettermodal" id="duvine-newslettermodal">
        <div class="l-container l-container--small d-column-container newsletter-modal-container">
            <div class="d-col d-col--1-3 newsletter-img" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/newsletter-signup.jpg);"></div>
            <div class="d-col d-col--2-3 newsletter-content">
              <div class="close"></div>
              <h4>Want some DuVine inspiration?</h4>
              <h3 class="blockheader">Sign up for our newsletter:</h3>
              <div class="footer-newsletter__form">
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>
                <script>
                  hbspt.forms.create({
                    css: '',
                    portalId: '408217',
                    formId: '8f9177f0-0d14-4b9a-8ddf-7b2731274bb4',
                    onFormSubmit: function($form) {
                        Cookies.set('duvine-newsletter', '1', { expires: 365 });
                    }
                  });
                </script>
              </div>
            </div>
        </div>
        <a href="#" class="closesearch"></a>
    </div>
<?php
}
endif; // duvine_render_newsletter_modal




if( !function_exists( 'duvine_render_optional_itinerary' ) ) :
/**
 * Renders the pre and post tour optional days/itineraries
 *
 * @param boolean $pretour // whether this is the posttour itinerary or not
 */
function duvine_render_optional_itinerary( $preOrPost = null ){
    if( $preOrPost === null || ($preOrPost !== 'pre' && $preOrPost !== 'post' )  ){
        error_log( print_r( "No optional itinerary indicated.", true ) );
        return false;
    }

    global $post;

    //$phoneNumber = str_replace(' ', '-', duvine_get_phone_number());
    $calltobook = '<p><strong>To reserve call ' . duvine_get_phone_number() . '</strong></p>';

    $titleField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_title' : 'd_itinerary_post_tour_title';
    $imageField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_image' : 'd_itinerary_post_tour_image';
    $descriptionField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_description' : 'd_itinerary_post_tour_description';
    $priceField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_price' : 'd_itinerary_post_tour_price';
    $priceField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_price' : 'd_itinerary_post_tour_price';
    $durationField = $preOrPost === 'pre' ? 'd_itinerary_pre_tour_duration' : 'd_itinerary_post_tour_duration';

    $graybox_title = $preOrPost === 'pre' ? 'Pre-tour' : 'Post-tour' ;


    $title = sk_get_field($titleField, array(
        'before'  => '<h4 class="accordion__title">',
        'after'   => '</h4>',
        'default' => "Pre tour option"
    ));
    $price = sk_get_field($priceField, array(
        'before' => '<p><strong>Price Per Person: </strong>',
        'after'  => '</p>',
        'filter' => 'format_price'
    ));
    $image = sk_get_field($imageField, array(
        'before' => '<div class="itinerary__dayimage">',
        'after'  => '</div>',
        'filter' => 'sk_img_markup'
    ));
    $description = sk_get_field($descriptionField, array(
        'before' => '<div class="itinerary__daydescription">',
        'after'  => $price . $calltobook . '</div>'
    ));


    // title
    $header = '<div class="itinerary__title">';
    $header .=  '<div class="itinerary__daymarker itinerary__daymarker--gray">' . $graybox_title . '</div>';
    $header .=  $title;
    $header .= '</div>';

    // body
    $content = '<div class="itinerary__daywrapper">' . $description . $image . '</div>';

    
    duvine_accordion_element( $header, $content );

}
endif; // duvine_render_optional_itinerary




if( !function_exists( 'duvine_render_itinerary_day' ) ) :
/**
 * Renders the itinerary day
 */
function duvine_render_itinerary_day( $daynumber = 0 ){

    $dayDescription = sk_get_subfield('description', array(
        'before' => '<div class="itinerary__daydescription">',
        'after'  => '</div>'
    ) );
    $dayImage = sk_get_subfield('image', array(
        'before' => '<div class="itinerary__dayimage">',
        'after'  => '</div>',
        'filter' => 'sk_img_markup'
    ) );


    // the header
    $dayBlock = "<div class=\"itinerary__daymarker\">Day <span>$daynumber</span></div>";
    $dayTitle = sk_get_subfield('title', array(
        'before'  => "<div class=\"itinerary__title\">$dayBlock<h4 class=\"accordion__title\">",
        'after'   => '</h4></div>',
        'default' => "Day $daynumber"
    ) );

    // build the day content
    $dayContent = '<div class="itinerary__daywrapper">' . $dayDescription . $dayImage . '</div>';

    duvine_accordion_element( $dayTitle, $dayContent );
    
}
endif; // duvine_render_itinerary_day




/* -------------------------------------------------
 *
 * --getters
 *     - functions that return things
 *
 * ------------------------------------------------- */




if( !function_exists( 'duvine_get_tour_dates' ) ) :
/**
 * Gets and returns the tour dates
 *
 * @return array
 */
function duvine_get_tour_dates(){
    global $post;

    $dates = get_field('d_tour_schedules');
    $today = date('Ymd');

    $displayDates = array();

    if( $dates ){
        foreach( $dates as $date ){

            $start = $date['start_date'];

            if( $start > $today ){
                $start = strtotime( $start );
                $end = strtotime( $date['end_date'] );

                $startYear = date('Y', $start);
                $endYear = date('Y', $start);

                $startMonth = date('M', $start);
                $endMonth = date('M', $end);

                $startDay = date('j', $start);
                $endDay = date('j', $end);

                // construct the string
                $dateString = "$startMonth $startDay &ndash; ";

                if( $startMonth !== $endMonth ){
                    $dateString .= "$endMonth ";
                }

                $dateString .= $endDay;

                if( $startYear !== $endYear ){
                    $dateString .= " $endYear";
                }

                $dateArray = array(
                    'start_date' => date( 'm/d/Y', $start ),
                    'label'      => $dateString,
                    'price'      => $date['price'],
                    'events'     => $date['special_events'],
                    'flags'      => $date['date_flags']
                );

                if( isset( $displayDates[$startYear] ) ){
                    array_push($displayDates[$startYear], $dateArray);
                } else {
                    $displayDates[ $startYear ] = array( $dateArray );
                }
            }
        }
    }

    ksort( $displayDates );

    return $displayDates;
}
endif; // duvine_get_tour_dates





if( !function_exists( 'duvine_get_hierarchical_location_list' ) ) :
/**
 * Gets the locations that all tours go, grouped hierarchically
 */
function duvine_get_hierarchical_location_list(){

    //delete_transient( 'duvine_hierarchical_location_list' );

    if( false === ( $locationlist = get_transient( 'duvine_hierarchical_location_list' ) ) ){
        $locationlist = array();

        $continents = get_posts( array(
            'post_type'      => 'location',
            'posts_per_page' => -1,
            'orderby'        => 'menu_order',
            'order' => 'ASC',
            'post_parent'    => 0
        ));

        if( ! $continents ){
            return $locationlist;
        }

        foreach( $continents as $continent ){
            $continentData = array(
                'continent' => $continent->post_title,
                'slug'      => $continent->post_name,
                'id'        => $continent->ID
            );

            $countries = get_posts( array(
                'post_type'      => 'location',
                'posts_per_page' => -1,
                'orderby'        => 'menu_order',
                'order' => 'ASC',
                'post_parent'    => $continent->ID
            ));

            if( $countries ){
                $continentData['countries'] = $countries;
            }

            array_push($locationlist, $continentData);
        }

        set_transient( 'duvine_hierarchical_location_list', $locationlist, 60 * 60 );
    }

    return $locationlist;
}
endif; // duvine_get_hierarchical_location_list







if( !function_exists('duvine_get_minimum_price') ) :
/**
 * Gets the miniumum price of the given tour
 *
 * @return string
 */
function duvine_get_minimum_price(){
    global $post;

    if( $startingPrice = get_field('d_private_starting_price') ){
        return apply_filters('format_price', $startingPrice);
    }

    $dates = get_field('d_tour_schedules');

    if(!$dates){
        return '';
    }

    $min = $dates[0]['price'];

    if(!$min){
        return '';
    }

    foreach( $dates as $date ){
        if( $date['price'] && $date['price'] < $min ){
            $min = $date['price'];
        }
    }

    return apply_filters('format_price', $min);
}
endif; // duvine_get_minimum_price





if( !function_exists( 'duvine_get_locationlist' ) ) :
/**
 * Gets the list of locations (the content type) from the WordPres cms
 *
 * @return array
 */
function duvine_get_locationlist(){

    //delete_transient( 'duvine_location_select_list' );

    if( false === ( $locationlist = get_transient( 'duvine_location_select_list' ) ) ){

        $locationlist = array();

        $locations = get_posts( array(
            'post_type'      => 'location',
            'posts_per_page' => -1,
            'orderby'        => 'menu_order',
            'order'         => 'ASC',
        ));

        if( ! $locations ){
            return false;
        }

        foreach( $locations as $loc ){
            $locationData = array(
                'key' => $loc->post_title,
                'id'  => $loc->ID
            );

            $parentId = $loc->post_parent;
            $locationLabel = $loc->post_title;

            while( $parentId !== 0 ) {
                $parent = get_post( $parentId );

                if( $parent->post_parent === 0 ){
                    $locationLabel .= htmlentities(' <span class="destination__continent">in ' . $parent->post_title . '</span>');
                } else {
                    $locationLabel .= ', ' . $parent->post_title;
                }

                $parentId = $parent->post_parent;
            }

            $locationData['label'] = $locationLabel;

            array_push( $locationlist, $locationData );
        }

        set_transient( 'duvine_location_select_list', $locationlist, 60 * 60 );
    }

    return $locationlist;
}
endif; // duvine_get_locationlist





if( !function_exists( 'duvine_get_centaur_link' ) ) :
/**
 * Retrieves and returns the link to the centaur booking site
 *
 * @param string $centaurId ID of the tour
 * @param string $startDate Starting date of the tour
 *
 * @return string | false (if no $centaurId)
 */
function duvine_get_centaur_link( $centaurId, $startDate ){

    if( !$centaurId ){
        return false;
    }

    $url = "https://centaur.duvine.com/centaur6/online/OPR_departureDateSelect?tourId=$centaurId&company=dvi&depDate=$startDate&sFlag=true";

    return $url;
}
endif; // duvine_get_centaur_link




if( !function_exists( 'duvine_get_url_from_object' ) ) :
/**
 * Gets the URL for an image from an image object.
 *
 * @param object | string $img      The image data from the database, or ID
 * @param string          $imgSize  The renderable size of the image
 *
 * @return string | false if no image
 */
function duvine_get_url_from_object( $img, $imgSize = 'full' ){
    if( ! $img ){
        return false;
    }

    $id = is_array($img) ? $img['ID'] : $img;

    $imgSrc = wp_get_attachment_image_src( $id, $imgSize );
    if( $imgSrc ){
        return $imgSrc[0];
    }

    return false;

}
endif; // duvine_get_url_from_object





if( !function_exists( 'duvine_get_phone_number' ) ) :
/**
 * Gets the phone number
 */
function duvine_get_phone_number(){

    $contact = get_field('sk_contact_phone', 'option');

    if( $contact ){
        return $contact;
    }

    return false;
}
endif; // duvine_get_phone_number






if( !function_exists( 'duvine_get_office_hours' ) ) :
/**
 * Gets the phone number
 */
function duvine_get_office_hours(){
    $hours = get_field('sk_contact_office_hours', 'option');

    if( $hours ){
        return '<div class="footercontact">' . $hours . '</div>';
    }

    return false;
}
endif; // duvine_get_office_hours






if( !function_exists( 'duvine_organize_tours_by_location' ) ) :
/**
 * Takes a list of posts and organizes an array of posts organized by tour location
 *
 * @param array $posts List of WP posts
 * @param int   $depth Location depth
 *
 * @return array
 */
function duvine_organize_tours_by_location( $posts, $depth ){
    $organized = array();

    if( !$posts ){
        return false;
    }

    foreach( $posts as $tour ){
        $locations = get_field('d_tour_location', $tour->ID);

        // $loc = duvine_get_first_level_location( $locations );
        $loc = duvine_get_location_at_depth( $locations, $depth );

        if( isset( $organized[ $loc ] ) && $organized[ $loc ] ){
            array_push( $organized[ $loc ], $tour );
        } else {
            $organized[ $loc ] = array( $tour );
        }
    }

    return $organized;    
}
endif; // duvine_organize_tours_by_location








if( !function_exists( 'duvine_get_location_at_depth' ) ) :
/**
 * Given a list of locations, return the location whose parent is a continent
 *
 * @param array $locations List of Locations within WordPress
 * @param int   $depth     Location depth
 *
 * @return array
 */
function duvine_get_location_at_depth( $locations, $depth = 1 ){
    if( !$locations ){
        return 0;
    }

    foreach( $locations as $loc ){
        $locObj = get_post($loc);
        $ancestors = get_ancestors($locObj->ID, 'location', 'post_type');

        if( count( $ancestors ) + 1 === $depth ){
            return $loc;
        }
    }

    return $locations[0];
}
endif; // duvine_get_location_at_depth








if( !function_exists( 'duvine_get_press_url' ) ) :
/**
 * Gets the appropriate press url (downloadable asset vs. website)
 *
 * @param int | object $press, The press content from WordPress
 *
 * @return string, the url to the press content
 */
function duvine_get_press_url( $press = null ){
    if( $press === null ){
        global $post;
        $press = $post;
    }

    if( is_object( $press ) && isset( $press->ID ) ){
        $id = $press->ID;
    } else {
        $id = $press;
    }

    $pressUrl = get_field('d_press_downloadable_asset', $id);
    if( $pressUrl ){
        return $pressUrl['url'];
    }

    $pressUrl = get_field('d_press_url', $id);
    if( $pressUrl ){
        return $pressUrl;
    }

    return '#';

}
endif; // duvine_get_press_url





if( !function_exists( 'duvine_get_active_filters' ) ) :
/**
 * Gets the relevatnt filters that are being filtered by for the given post
 *
 * @param object $post Tour post
 * @return string
 */
function duvine_get_active_filters( $post ){

    $taxfilter_culinary  = get_query_var('taxfilter_culinary');
    $taxfilter_culture   = get_query_var('taxfilter_culture');
    $taxfilter_landscape = get_query_var('taxfilter_landscape');
    $taxfilter_activity  = get_query_var('taxfilter_activity');


    $termCount = 0;
    $terms = 'Includes:';

    if( $taxfilter_culinary ){
        if( $post_culinary = wp_get_post_terms( $post->ID, 'culinary' ) ){
            foreach( $post_culinary as $term ){
                if( in_array($term->term_id, $taxfilter_culinary) ){
                    if( $termCount > 0 ){
                        $terms .= ',';
                    }
                    $termCount++;
                    $terms .= ' ' . $term->name;
                }
            }
        }
    }

    if( $taxfilter_culture ){
        if( $post_culture = wp_get_post_terms( $post->ID, 'culture' ) ){
            foreach( $post_culture as $term ){
                if( in_array($term->term_id, $taxfilter_culture) ){
                    if( $termCount > 0 ){
                        $terms .= ',';
                    }
                    $termCount++;
                    $terms .= ' ' . $term->name;
                }
            }
        }
    }

    if( $taxfilter_landscape ){
        if( $post_landscape = wp_get_post_terms( $post->ID, 'landscape' ) ){
            foreach( $post_landscape as $term ){
                if( in_array($term->term_id, $taxfilter_landscape) ){
                    if( $termCount > 0 ){
                        $terms .= ',';
                    }
                    $termCount++;
                    $terms .= ' ' . $term->name;
                }
            }
        }
    }

    if( $taxfilter_activity ){
        if( $post_activity = wp_get_post_terms( $post->ID, 'activity' ) ){
            foreach( $post_activity as $term ){
                if( in_array($term->term_id, $taxfilter_activity) ){
                    if( $termCount > 0 ){
                        $terms .= ',';
                    }
                    $termCount++;
                    $terms .= ' ' . $term->name;
                }
            }
        }
    }

    if( $termCount > 0 ){
        return $terms;
    }

    return '';
}
endif; // duvine_get_active_filters





if( !function_exists( 'duvine_get_tooltip' ) ) :
/**
 * Gets and returns a tooltip
 *
 * @param (string) $field
 *
 * @return string
 */
function duvine_get_tooltip( $field, $color ){
    
    $content = get_field($field, 'option');

    if( ! $content ){
        return '';
    }

    if( $color == 'dark' ) :
        $tooltip_icon = '/wp-content/themes/sherman/svg/question-mark.svg';
    else :
        $tooltip_icon = '/wp-content/themes/sherman/svg/question-mark-light.svg';
    endif;

    $tooltip_markup = "<img src=\"$tooltip_icon\" class=\"tooltip $color\" data-tooltip-content=\"#$field\" width=\"23px\" />";
    $tooltip_markup .= "<div class=\"tooltip_templates\" style=\"display:none\"><div id=\"$field\">$content</div></div>";

    return $tooltip_markup;
}
endif; // duvine_get_tooltip




/* -------------------------------------------------
 *
 * --util
 *
 * ------------------------------------------------- */


if(!function_exists('duvine_is_family_tour')) :
/**
 * checks whether or not the current tour is a family tour or not
 * 
 * @param {array} $collections // (optional) A list of tour collections
 * @return boolean
 */
function duvine_is_family_tour($collections = null){
    global $post;

    if($collections === null){
        $collections = get_field('d_tour_collection');
    }

    return in_array(180, $collections);
}
endif; // duvine_is_family_tour




if( !function_exists( 'duvine_build_tour_queryargs' ) ) :
/**
 * Builds the WP_Query args for a tourloop
 *
 * @param array $args    The WP_Query arguments
 * @param array $filters List of filters to build a query
 */
function duvine_build_tour_queryargs( $args = array() ){
    
    $defaults = array(
        'posts_per_page' => 10,
        'post_type'      => 'tour',
        'meta_query'     => array(),
        'tax_query'      => array(),
        'post_status'    => 'publish',

        // 'meta_key'       => 'd_tour_location',
        // 'orderby'        => array( 'meta_value' => 'ASC', 'title' => 'ASC'),
        'orderby'        => array( 'title' => 'ASC'),
    );
    $queryArgs = array_merge( $defaults, $args );


    $q_destination         = get_query_var( 'destination' );
    $q_level               = get_query_var( 'level' );
    $q_collection          = get_query_var( 'collection' );
    $q_taxfilter_landscape = get_query_var( 'taxfilter_landscape' );
    $q_taxfilter_activity  = get_query_var( 'taxfilter_activity' );
    $q_taxfilter_culinary  = get_query_var( 'taxfilter_culinary' );
    $q_taxfilter_culture   = get_query_var( 'taxfilter_culture' );
    $q_date                = get_query_var( 'date' );
    $q_tourtype            = get_query_var( 'tourtype' );
    $q_duration            = get_query_var( 'duration' );

    if( $q_destination && ! is_array( $q_destination )){
        $q_destination = array( $q_destination );
    }


    $meta_query = array_merge( array( 'relation' => 'AND' ), $queryArgs['meta_query'] );
    $tax_query = array_merge( array( 'relation' => 'OR' ), $queryArgs['tax_query'] );

    if( $q_destination ){
        $destination_meta_query = array( 'relation' => 'OR' );
        foreach( $q_destination as $dest ){
            array_push( $destination_meta_query, array(
                'key'     => 'd_tour_location',
                'value'   => '"' . $dest . '"',
                'compare' => 'LIKE'
            ));
        }
        array_push( $meta_query, $destination_meta_query );
    }

    if($q_duration) {

        $duration_meta_query = array( 'relation' => 'OR' );
        foreach( $q_duration as $duration ){
            
            if($duration == '4') {
                array_push( $duration_meta_query, array(
                    'key'     => 'd_tour_duration',
                    'value'   => '^[4][ ]',
                    'compare' => 'REGEXP'
                ));
            } else {
                array_push( $duration_meta_query, array(
                    'key'     => 'd_tour_duration',
                    'value'   => array( 5, 50 ),
                    'type'    => 'numeric',
                    'compare' => 'BETWEEN',
                ));
            }
        }
        array_push( $meta_query, $duration_meta_query );
    }

    if( $q_level ){
        $levels_meta_query = array('relation' => 'OR');
        foreach( $q_level as $level ){
            if( $level > 0 ){
                array_push( $levels_meta_query, array(
                    'key'     => 'd_cycling_level',
                    'value'   => $level,
                    'compare' => 'LIKE'
                ));
            } else {
                array_push( $levels_meta_query, array(
                    'key'     => 'd_non_rider',
                    'value'   => '1',
                    'compare' => '=='
                ));
            }
            
        }
        array_push( $meta_query, $levels_meta_query );
    }


    if( $q_collection ){
        $collections_meta_query = array('relation' => 'OR');
        foreach( $q_collection as $collection ){
            array_push( $collections_meta_query, array(
                'key'     => 'd_tour_collection',
                'value'   => '"' . $collection . '"',
                'compare' => 'LIKE'
            ));
        }
        array_push( $meta_query, $collections_meta_query );
    }


    if( $q_date ){
        $dates_meta_query = array( 'relation' => 'OR', array(
            'key'     => 'd_private_only',
            'value'   => 1,
            'compare' => 'LIKE'
        ) );

        foreach( $q_date as $date ){
            array_push( $dates_meta_query, array(
                'key'     => 'd_tour_schedules_%_start_date',
                'value'   => '^' . $date,
                'compare' => 'REGEXP'
            ));
        }

        array_push( $meta_query, $dates_meta_query );
    }



    if( $q_taxfilter_landscape ){
        array_push( $tax_query, array(
            'taxonomy' => 'landscape',
            'field'    => 'term_id',
            'terms'    => $q_taxfilter_landscape
        ));
    }
    if( $q_taxfilter_activity ){
        array_push( $tax_query, array(
            'taxonomy' => 'activity',
            'field'    => 'term_id',
            'terms'    => $q_taxfilter_activity
        ));
    }
    if( $q_taxfilter_culinary ){
        array_push( $tax_query, array(
            'taxonomy' => 'culinary',
            'field'    => 'term_id',
            'terms'    => $q_taxfilter_culinary
        ));
    }
    if( $q_taxfilter_culture ){
        array_push( $tax_query, array(
            'taxonomy' => 'culture',
            'field'    => 'term_id',
            'terms'    => $q_taxfilter_culture
        ));
    }


    if( $q_tourtype ){
        if( $q_tourtype === 'scheduled' ){
            array_push( $meta_query, array(
                'key'     => 'd_private_only',
                'value'   => '1',
                'compare' => '!='
            ));
        } elseif( $q_tourtype === 'private' ){
            array_push( $meta_query, array(
                'key'     => 'd_private_only',
                'value'   => '1',
                'compare' => '=='
            ));
        }
    }
    
    // debug
    // array_push( $meta_query, array(
    //     'key'     => 'd_new_tour',
    //     'value'   => '1',
    //     'compare' => '=='
    // ));



    // merge all the meta and taxonomy queries
    $queryArgs['meta_query'] = $meta_query;
    $queryArgs['tax_query'] = $tax_query;

    return $queryArgs;


}
endif; // duvine_build_tour_queryargs





if( !function_exists( 'duvine_get_post_by_slug' ) ) :
/**
 * Gets post by slug
 *
 * @param string $post_name Slug to search
 * @param string $post_type Type of post
 */
function duvine_get_post_by_slug( $post_name, $post_type = 'post' ){
    
    $post_ids = get_posts(array (
        'name'        => $post_name,
        'post_type'   => $post_type,
        'numberposts' => -1
    ));

    if( $post_ids ){
        return $post_ids[0]->ID;
    }

    return false;
}
endif; // duvine_get_post_by_slug