// -----------------------------------------------
// --Instaslider
//
// The functionality for the instaslider
//
;(function( DUVINE, $, undefined ){

    var Instaslider = (function(){

        var

            CAN_AJAX = typeof duvine_ajax === 'object' && duvine_ajax.ajaxurl,

            PREVARROW = '<span class="sk-slider-nav nav-previous"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 612 792"><polyline points="427.9,646.4 168.2,389.4 427.9,129.7 "/></svg></span>',
            NEXTARROW = '<span class="sk-slider-nav nav-next"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 612 792"><polyline points="168.2,129.7 427.9,386.7 168.2,646.4 "/></svg></span>',

            TIMING = 600;

        


        // ---------------
        // PUBLIC API
        //



        /**
         * init the things
         */
        function Instaslider( el ){
            this.el = el;
            this.$el = $(el);

            this.instaTarget = this.$el.data('id') || 'duvine-instagram';
            this.$sliderContainer = this.$el.find('#' + this.instaTarget);
            this.instaTag = this.$el.attr('data-tag') || '';
            
            if( this.$sliderContainer.length === 0 || this.$sliderContainer.hasClass('insta__list--fromcache') ){
                this.init_carousel();
                return false;
            }

            // instafeed keys
            this.userId = 25164000;
            this.clientId = '9bc08525f4fb49ad97daeb7b3a575164';
            this.accessToken = '25164000.1677ed0.8ddb17265af143258efde82bf5c5df14';


            // build
            this.build();
        }




        //
        // PROTOTYPE
        //


        /**
         * Build it
         */
        Instaslider.prototype.build = function(){

            var _this = this;

            var imgs = [];

            var tag = this.instaTag;

            if( tag != 'duvine' && tag.length > 0) {
                var tag = this.instaTag;
                var feed = new Instafeed({
                    get         : 'user',
                    userId      : this.userId,
                    clientId    : this.clientId,
                    accessToken : this.accessToken,
                    sortBy      : 'none',
                    // resolution  : 'thumbnail',
                    resolution  : 'standard_resolution',
                    limit       : 60, /* increase to 60 for tags */
                    template    : '<li class="insta__post"><a class="insta__postimg" href="{{link}}" target="_blank" style="background-image: url({{image}})"></a></li>',
                    target      : this.instaTarget,
                    filter: function(image) {
                        return image.tags.indexOf(tag) >= 0;
                    },
                    after       : function () {

                        if( !CAN_AJAX ){
                            _this.init_carousel();
                            return false;
                        }

                        var markup = $.trim( _this.$sliderContainer.html() );

                        _this.init_carousel();

                        $.ajax({
                            url  : duvine_ajax.ajaxurl,
                            type : 'POST',
                            data : {
                                action : 'save_instagram_markup',
                                markup : encodeURIComponent( markup ),
                            }
                        }).done( function(){
                            console.log("Instagram tag data cached.");
                        } ).fail( function ( jqXHR, textStatus, error ) {
                            console.error("error");
                            console.error( error );
                        } );
                    }

                });

            } else {

                console.log('this is duvine');
                var feed = new Instafeed({
                    get         : 'user',
                    userId      : this.userId,
                    clientId    : this.clientId,
                    accessToken : this.accessToken,
                    sortBy      : 'most-recent',
                    // resolution  : 'thumbnail',
                    resolution  : 'standard_resolution',
                    limit       : 8,
                    template    : '<li class="insta__post"><a class="insta__postimg" href="{{link}}" target="_blank" style="background-image: url({{image}})"></a></li>',
                    target      : this.instaTarget,
                    /*filter: function(image) {
                        return image.tags.indexOf('duvineapresvelo') >= 0;
                    },*/
                    after       : function () {

                        if( !CAN_AJAX ){
                            _this.init_carousel();
                            return false;
                        }

                        var markup = $.trim( _this.$sliderContainer.html() );

                        _this.init_carousel();

                        $.ajax({
                            url  : duvine_ajax.ajaxurl,
                            type : 'POST',
                            data : {
                                action : 'save_instagram_markup',
                                markup : encodeURIComponent( markup ),
                            }
                        }).done( function(){
                            console.log("Instagram data cached.");
                        } ).fail( function ( jqXHR, textStatus, error ) {
                            console.error("error");
                            console.error( error );
                        } );
                    }

                });
            }

            feed.run();
        }




        /**
         * Inits the carousel
         */
        Instaslider.prototype.init_carousel = function(){

            if( typeof $().slick !== 'function' ){
                console.error( "Slick is not initialized." );
                return false;
            }

            this.$sliderContainer.slick({
                centerMode    : true,
                centerPadding : '60px',
                slidesToShow  : 6,
                prevArrow     : PREVARROW,
                nextArrow     : NEXTARROW,
                responsive    : [{
                   breakpoint : 1780,
                   settings   : {
                       slidesToShow: 5
                   } 
                },{
                    breakpoint : 1060,
                    settings   : {
                        slidesToShow: 4
                    }
                },{
                    breakpoint : 800,
                    settings   : {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint : 560,
                    settings   : {
                        slidesToShow: 2
                    }
                }, {
                    breakpoint : 420,
                    settings   : {
                        slidesToShow: 1
                    }
                }]
            });
        }



        return Instaslider;

    })();




    // -------------------------------
    // DOM ready
    //
    $(document).ready(function(){
        $('.instaslider').each( function(){
            var $this         = $(this),
                instaslider = $this.data('instaslider');

            if( !(instaslider instanceof Instaslider ) ){
                $this.data('instaslider', new Instaslider( this ) );
            }
        });
    });

})( window.DUVINE = window.DUVINE || {}, jQuery );