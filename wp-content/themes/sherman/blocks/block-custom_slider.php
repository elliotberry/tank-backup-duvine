<?php 
/**
 * MODULE - Custom Slider
 *
 * Slider of custom content and/or blog posts
 *
 * @subfield custom_slider_items (repeater)
 *
 */
?>

<?php if( have_rows('custom_slider_items') ): ?>
        <div class="l-container">
            <div class="featuredblogs">
                <?php while ( have_rows('custom_slider_items') ) : the_row(); ?>

                    <div class="featuredblog__slide">

                    <?php if(get_sub_field('type') == 'custom-post') { ?>

                        <?php if(get_sub_field('image_or_video') == 'video') : ?>

                            <?php if(get_sub_field('video')) : ?>
                                <div class="featuredblog__video">
                                    <video data="<?php echo get_sub_field('video'); ?>" playsinline loop controls id="vid1"  onended="this.play();" poster="<?php the_sub_field('video_poster'); ?>">
                                        <source src="<?php echo get_sub_field('video'); ?>" type="video/mp4">
                                     </video>
                                </div>
                            <?php endif; ?>

                        <?php else : ?>

                            <?php if(get_sub_field('image')) : ?>
                                <div class="featuredblog__image" style="background-image:url(<?php the_sub_field('image'); ?>);"></div>
                            <?php endif; ?>

                        <?php endif; ?>

                        <div class="featuredblog__content">
                            <header class="featuredblog__header">
                                <h3 class="blockheader blockheader--white"><?php echo get_sub_field('title'); ?></h3>
                            </header>

                            <?php if(get_sub_field('text')) { ?>
                                <div class="featuredblog__summary">
                                    <?php the_sub_field('text'); ?>
                                </div>
                            <?php } ?>

                            <?php if(get_sub_field('link')) { ?>
                            <a href="<?php the_sub_field('link'); ?>" class="cta cta--hoverwhite">Read More</a>
                            <?php } ?>
                        </div>

                    <?php } else { ?>

                        <?php $blogpost = get_sub_field('blog_post'); ?>

                        <?php
                        $blog_featured_image = sk_get_field('d_blog_featured_image', array(
                            'id'          => $blogpost->ID,
                            'filter'      => 'sk_background_image',
                            'filter_args' => array(
                                'img_size' => 'banner_hero_page'
                            )
                        ));
                        ?>
                        <?php if($blog_featured_image) : ?>
                            <div class="featuredblog__image" style="<?php echo $blog_featured_image; ?>"></div>
                        <?php endif; ?>
                        
                        <div class="featuredblog__content">
                        <header class="featuredblog__header">
                            <h3 class="blockheader blockheader--white"><?php echo $blogpost->post_title; ?></h3>
                        </header>

                        <?php
                            sk_the_field('d_blog_featured_summary', array(
                                'before' => '<div class="featuredblog__summary">',
                                'after'  => '</div>',
                                'id'     => $blogpost->ID
                            ));
                        ?>

                        <a href="<?php echo get_the_permalink( $blogpost->ID ); ?>" class="cta cta--hoverwhite">Read More</a>
                    </div> 

                    <?php } ?>

                    </div>              

                <?php endwhile; ?>
            </div>
        </div>
<?php endif; ?>