<?php 
/**
 * MODULE - Featured blog posts
 *
 * Features any number of blog posts and places them in a slider.
 *
 * @subfield featured_posts (relationship)
 *
 */
?>

<?php if( $featuredPosts = get_sub_field('featured_posts') ) : ?>
    <div class="l-container">

        <div class="featuredblogs">
            <?php foreach( $featuredPosts as $blogpost ) : ?>
                <div class="featuredblog__slide">
                    <?php
                        $blog_featured_image = sk_get_field('d_blog_featured_image', array(
                            'id'          => $blogpost->ID,
                            'filter'      => 'sk_background_image',
                            'filter_args' => array(
                                'img_size' => 'banner_hero_page'
                            )
                        ));
                    ?>
                    <?php if($blog_featured_image) : ?>
                        <div class="featuredblog__image" style="<?php echo $blog_featured_image; ?>"></div>
                    <?php endif; ?>

                    <div class="featuredblog__content">
                        <header class="featuredblog__header">
                            <h3 class="blockheader blockheader--white"><?php echo $blogpost->post_title; ?></h3>
                        </header>

                        <?php
                            sk_the_field('d_blog_featured_summary', array(
                                'before' => '<div class="featuredblog__summary">',
                                'after'  => '</div>',
                                'id'     => $blogpost->ID
                            ));
                        ?>

                        <a href="<?php echo get_the_permalink( $blogpost->ID ); ?>" class="cta cta--hoverwhite">Read More</a>
                    </div>              
                </div>
            <?php endforeach; ?>
        </div>        

    </div>
<?php endif; ?>