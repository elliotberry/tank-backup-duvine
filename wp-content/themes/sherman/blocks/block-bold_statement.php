<?php 
/**
 * MODULE - Bold Statement
 *
 * Block with large text and a call to action
 *
 * @subfield statement (text area)
 * @subfield call_to_action (clone)
 *
 */


// sk_the_subfield('copy');

?>


<div class="boldstatement l-container">
    <?php
        sk_block_field('statement', array(
            'before' => '<div class="boldstatement__statement">',
            'after'  => '</div>',
            'filter' => 'smart_type'
        ));

        $award_text = sk_get_subfield('award_text', array(
            'before' => '<div class="boldstatement__awardtext">',
            'after'  => '</div>'
        ));

        $award_image = sk_get_subfield('award_logo', array(
            'before' => '<div class="boldstatement__awardimage">',
            'after'  => '</div>',
            'filter' => 'sk_img_markup'
        ));

        if( $award_text || $award_image ){
            echo '<div class="boldstatement__award">';
            echo $award_text;
            echo $award_image;
            echo '</div>';
        }

        duvine_render_cta();
    ?>
</div>