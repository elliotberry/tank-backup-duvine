<?php 
//
// MODULE - Tour Browser
//
// Browse for tours
//


$q_destinations  = get_query_var( 'destination' );         // d_tour_location
$q_levels        = get_query_var( 'level' );               // d_cycling_level
$q_collections   = get_query_var( 'collection' );          // d_tour_collection
$q_tax_landscape = get_query_var( 'taxfilter_landscape' ); // d_tax_landscape
$q_tax_activity  = get_query_var( 'taxfilter_activity' );  // d_tax_activities
$q_tax_culinary  = get_query_var( 'taxfilter_culinary' );  // d_tax_culinary_experiences
$q_tax_culture   = get_query_var( 'taxfilter_culture' );   // d_tax_cultural_experiences
$q_dates         = get_query_var( 'date' );
$q_tourtype      = get_query_var( 'tourtype' );
$q_duration      = get_query_var( 'duration' );

$otherFiltersCount = 0;

if( $q_tax_landscape ){
    $otherFiltersCount += count( $q_tax_landscape);
}
if( $q_tax_activity ){
    $otherFiltersCount += count( $q_tax_activity);
}
if( $q_tax_culinary ){
    $otherFiltersCount += count( $q_tax_culinary);
}
if( $q_tax_culture ){
    $otherFiltersCount += count( $q_tax_culture);
}

?>

<div class="l-container tourloop">
    <header class="headerpromo__wrapper">
        <?php sk_the_subfield('title', array('before' => '<h1 class="superheader headerpromo__title">', 'after' => '</h1>')); ?>
    </header>

    <a class="tourfilter__mobiletrigger" href="#">Filter</a>

    <form class="tourfilters" action="<?php echo get_permalink(); ?>">
        <section class="duvinefilters">

            <div class="tourfilters__container tourfilters__container--pad">
                <?php if( $locations = duvine_get_locationlist() ) : ?>
                    <div class="filtercell filtercell--destinations">
                        <span class="filtercell__label">Destinations</span>
                        
                        <select data-placeholder="Search by country or region" class="js-duvine-chosen" data-search="1" data-noresults="We don't travel there yet. Please search a new destination." name="destination[]" multiple>
                            <?php foreach( $locations as $location ) : ?>
                                <?php $selected = $q_destinations && in_array( $location['id'], $q_destinations ) ? ' selected' : ''; ?>
                                <option value="<?php echo $location['id']; ?>" data-markup="<?php echo $location['label']; ?>"<?php echo $selected; ?>><?php echo $location['key']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>



                <div class="filtercell filtercell--dates">
                    <span class="filtercell__label">Dates</span>
                    <?php duvine_date_picker_input(); ?>
                </div>
                
                <?php $tooltip_activity_level = duvine_get_tooltip( 'd_tooltip_activity_level', 'dark' ); ?>

                <div class="filtercell filtercell--level">
                    <span class="filtercell__label">Level <span class="tourmeta"><?php echo $tooltip_activity_level; ?></span></span>
                    <!--<span class="filtercell__label">Level</span>-->
                    <select data-placeholder="Select difficulty" class="js-duvine-chosen" multiple name="level[]">
                        <option value="1"<?php if( $q_levels && in_array(1, $q_levels) ) echo ' selected'; ?>>Level 1</option>
                        <option value="2"<?php if( $q_levels && in_array(2, $q_levels) ) echo ' selected'; ?>>Level 2</option>
                        <option value="3"<?php if( $q_levels && in_array(3, $q_levels) ) echo ' selected'; ?>>Level 3</option>
                        <option value="4"<?php if( $q_levels && in_array(4, $q_levels) ) echo ' selected'; ?>>Level 4</option>
                        <option value="0"<?php if( $q_levels && in_array(0, $q_levels) ) echo ' selected'; ?>>Non-rider option</option>
                    </select>
                </div>

                <?php
                    $tourCollections = get_posts( array(
                        'post_type'      => 'tour_collection',
                        'posts_per_page' => -1,
                        'post_parent'    => 0,
                        'orderby'        => array( 'menu_order' => 'ASC' )
                    ));
                ?>
                <?php if( $tourCollections) : ?>
                    <div class="filtercell filtercell--collection">
                        <span class="filtercell__label">Tour Collections</span>

                        <select data-placeholder="Select collection" class="js-duvine-chosen" multiple name="collection[]">
                            <?php foreach( $tourCollections as $collection ) : ?>
                                <?php $selected = $q_collections && in_array( $collection->ID, $q_collections ) ? ' selected' : ''; ?>
                                <option value="<?php echo $collection->ID; ?>"<?php echo $selected; ?>><?php echo $collection->post_title; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>

                <div class="filtercell filtercell--duration">
                    <span class="filtercell__label">Duration</span>

                    <select data-placeholder="Select length of time" class="js-duvine-chosen" multiple name="duration[]">
                        <option value="4" <?php if( $q_duration && in_array(4, $q_duration) ) echo ' selected'; ?>>4-day tours</option>
                        <option value="6" <?php if( $q_duration && in_array(6, $q_duration) ) echo ' selected'; ?>>6-day+ tours</option>
                    </select>
                </div>

                <div class="filtercell filtercell--morefilters">
                    <span class="tourfilters__filtercount<?php if( $otherFiltersCount > 0 ) echo ' tourfilters__filtercount--visible'; ?>"><?php echo $otherFiltersCount; ?></span>
                    
                    <a href="#" class="tourfilters__seemore js-trigger-advancedfilters">Filters</a>
                </div>

            </div>


            <div class="tourfilters__apply">
                <a href="#" class="basiclink filtercell__clearfilters js-clear-filters">Clear Filters</a>
                <a href="#" class="basiclink filtercell__closefilters js-close-mobilefilters">Cancel</a>
                <button class="cta" type="submit">Apply</button>
            </div>

            
            <div class="tourfilters__expandable">
                <div class="tourfilters__expandablepad">
                    <div class="tourfilters__container">
                        <?php duvine_list_tour_tags('activity', 'Activities'); ?>
                        <?php duvine_list_tour_tags('culinary', 'Culinary'); ?>
                        <?php duvine_list_tour_tags('culture', 'Cultural'); ?>
                        <?php duvine_list_tour_tags('landscape', 'Landscape'); ?>
                    </div>

                </div>
            </div>
        </section>

        <section class="tourloop__header">
            <div class="privatefilter">
                <span class="privatefilter__label">View:</span>

                <input type="radio" name="tourtype" id="tourtype--all" value="all"<?php if( $q_tourtype !== 'scheduled' && $q_tourtype !== 'private' ) echo 'checked'; ?>><label class="tourtype__label" for="tourtype--all">All Tours</label>

                <input type="radio" name="tourtype" id="tourtype--scheduled" value="scheduled"<?php if( $q_tourtype === 'scheduled' ) echo 'checked'; ?>><label class="tourtype__label" for="tourtype--scheduled">Scheduled Tours</label>

                <input type="radio" name="tourtype" id="tourtype--private" value="private"<?php if( $q_tourtype === 'private' ) echo 'checked'; ?>><label class="tourtype__label" for="tourtype--private">Private Only</label>
            </div>


            <div class="sortfilter">
                <span>SORT BY</span>
                <select class="js-duvine-chosen js-duvine-sortfilters">
                    <option value="title">Tour Name</option>
                    <option value="price">Price (low to high)</option>
                </select>
            </div>
        </section>
    </form>


    <section class="tourloop__section">        
        <div class="tourloop__holder">
            <?php duvine_render_tour_loop( array('duvine_show_count' => true, 'duvine_show_you_may_also_like' => true )); ?>
        </div>
    </section>

</div>