<?php
/*
Template Name: Wide Page
*/
/**
 * The template for displaying wide pages.
 *
 * @package sherman
 */

get_header(); ?>

    
    <?php if( !is_front_page() ) : ?>
        
        <?php duvine_render_page_hero(); ?>

        <div class="l-container breadcrumbs__pad">
            <ul class="breadcrumbs baselist">
                <li class="breadcrumb__item">
                <?php if( $post->post_parent ) : ?>
                    <a class="breadcrumb__link" href="<?php echo get_the_permalink( $post->post_parent); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>
                <?php else : ?>
                    <a class="breadcrumb__link" href="/">Home</a>
                <?php endif; ?>
                </li><li class="breadcrumb__item"><?php echo $post->post_title; ?></li>
            </ul>
        </div>
    <?php else : ?>
        <?php duvine_render_homepage_hero(); ?>
    <?php endif; ?>


    <div id="primary" class="content-area">
        
        <?php while ( have_posts() ) : the_post(); ?>
            <?php if( !get_field('d_hide_page_title') ) : ?>
                <header class="entry-header l-container">
                    <?php the_title( '<h1 class="superheader">', '</h1>' ); ?>
                </header><!-- .entry-header -->
            <?php endif; ?>

            <?php
                sk_the_field('d_page_intro_paragraph', array(
                    'before' => '<div class="pageintro l-container"><p>',
                    'after'  => '</p></div>'
                ));
            ?>

            <?php if ( '' !== $post->post_content ) : ?>
                <main id="main" class="site-main l-container d-content" role="main">
                    <?php get_template_part( 'content', 'page' ); ?>
                </main><!-- #main -->
            <?php endif; ?>

        <?php endwhile; // end of the loop. ?>

        <?php sk_the_page_blocks(); ?>

    </div><!-- #primary -->

<?php get_footer(); ?>
